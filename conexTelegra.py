import telebot
from chatterbot import ChatBot 
from chatterbot.trainers import ListTrainer
from chatterbot.response_selection import get_first_response
from chatterbot.comparisons import levenshtein_distance
import os


#TOKEN=("631923528:AAGRI1cMhYH2R5Qr3H8JaDIuRVBwUvDbW3A")
bot = telebot.TeleBot("631923528:AAGRI1cMhYH2R5Qr3H8JaDIuRVBwUvDbW3A")


def bot_conversacional(message):

    chatbot=ChatBot(
        #"Ejemplo Bot",trainer="chatterbot.traines.ChatterBotCorpusTrainer"
        "Chatterbot",storage_adapter="chatterbot.storage.SQLStorageAdapter",
                    preprocessor=['chatterbot.preprocessors.clean_whitespace'],

    logic_adapters=[
            
            {
                'import_path': 'chatterbot.logic.BestMatch',
                "statement_comparison_function":"chatterbot.comparisons.levenshtein_distance",
                "response_selection_method":"chatterbot.response_selection.get_first_response"


            },
            {
                'import_path': 'chatterbot.logic.LowConfidenceAdapter',
                'threshold': 0.65,
                'default_response': 'Esa información no esta en mi  base de datos pero mis programadores ya estan trabajando en eso.  '

            },
           ],
          trainer='chatterbot.trainers.ListTrainer'

        )
    
    respuesta=chatbot.get_response(message)
    respuesta=str(respuesta)
    #respuesta=("BOT:" + str(respuesta))

    mensaje=open("respuesta.txt","w")
    mensaje.write(respuesta)
    mensaje.close()

    for charlas in os.listdir('converbot/'):
        charlas=open('converbot/'+charlas,'r').readlines()
        chatbot.train(charlas)


    #chatbot.set_trainer(ListTrainer)
    #chatbot.train(charlas)


#Resibimos y enviamos una respuesta al comando star
@bot.message_handler(commands=["help","start"])
def envia_mensaje(message):
    bot.reply_to(message, "hola soy bot salud para ayudarte.")

#Resibimos cualquir otro mensaje

@bot.message_handler(func=lambda message:True)
def mensaje(message):

    bot_conversacional(message.text)
    respuesta=open("respuesta.txt","r")
    respuesta=respuesta.read()
    bot.reply_to(message,respuesta)

bot.polling()





